<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@page import="com.seta.training.dao.*" %>
<%@page import="com.seta.training.model.*" %>
<jsp:useBean id="address" class="com.seta.training.model.Address" scope="session"/>
<body>
<%
	Address addressInfo = AddressDAO.getUserAddress(address.getUserId());
 %>
 <br/>
 <div >Address info here</div>
 <%= addressInfo.getAddress() + " " + addressInfo.getCity() + " " + addressInfo.getCountry() %>
</body>
</html>