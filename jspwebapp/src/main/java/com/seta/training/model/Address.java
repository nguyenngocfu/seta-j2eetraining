package com.seta.training.model;

/*
 * @author: NgocNQ
 * Address model, has one to one relationship with User model
 */
public class Address {
	private int id;
	private int userId;
	private String address;
	private String city;
	private String state;
	private String country;
	
	public Address() {
		
	}
	
	public Address(int id, int userId, String address, String city,
			String state, String country) {
		super();
		this.id = id;
		this.userId = userId;
		this.address = address;
		this.city = city;
		this.state = state;
		this.country = country;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

}
