package com.seta.training.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
	public static final String DB_URL = "jdbc:mysql://localhost:3306/j2eetraining";
	public static final String DB_DRIVER_NAME = "com.mysql.jdbc.Driver";
	public static final String DB_USERNAME = "root";
	public static final String DB_PASSWORD = "";
	public static Connection getConnection() {
		try {
			// Register JDBC driver
			Class.forName(DB_DRIVER_NAME);
			
			// Open db connection and return the connection
			return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
		} catch(SQLException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
