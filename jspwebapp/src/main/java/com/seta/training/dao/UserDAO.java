package com.seta.training.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.seta.training.model.User;

public class UserDAO {
	
	public static User login(User user) {
		// build check user sql statement
		String checkUserQuery = new StringBuilder().append("Select * from user where user_name = '")
															.append(user.getUsername())
															.append("'")
															.toString();
		try {
			// Execute statements;
			Connection currentConnection = ConnectionManager.getConnection();
			Statement checkUserStatement = currentConnection.createStatement();
			ResultSet userSet = checkUserStatement.executeQuery(checkUserQuery);
			
			boolean hasNext = userSet.next();
			
			// Iterate each rows in database and check whether password match or not
			while(hasNext) {
				String userPass = userSet.getString("password");
				if(userPass.equals(user.getPassword())) {
					user.setUserId(userSet.getInt("user_id"));
					user.setUsername(userSet.getString("user_name"));
					user.setFirstName(userSet.getString("firstname"));
					user.setLastName(userSet.getString("lastname"));
					break;
				}
				hasNext = userSet.next();
			}
			
			// Close result set, statement and connection
			userSet.close();
			checkUserStatement.close();
			currentConnection.close();
			return user;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
