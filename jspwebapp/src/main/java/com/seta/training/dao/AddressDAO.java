package com.seta.training.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.seta.training.model.Address;

public class AddressDAO {

	public static Address getUserAddress(int userID) {
		// build check user sql statement
		String getAddressQuery = new StringBuilder()
				.append("Select * from address where user_id = '")
				.append(userID).append("'").toString();
		try {
			// Execute statements;
			Connection currentConnection = ConnectionManager.getConnection();
			Statement getAddressStatement = currentConnection.createStatement();
			ResultSet addressSet = getAddressStatement.executeQuery(getAddressQuery);

			boolean hasNext = addressSet.next();

			// Iterate each rows in database and check whether password match or
			// not
			Address userAddress = new Address();
			if (hasNext) {
				int id = addressSet.getInt("id");
				String address = addressSet.getString("address");
				String city = addressSet.getString("city");
				String state = addressSet.getString("state");
				String country = addressSet.getString("country");
				userAddress = new Address(id, userID, address, city, state, country);
			}
			
			// Close result set, statement and connection
			addressSet.close();
			getAddressStatement.close();
			currentConnection.close();
			return userAddress;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}
