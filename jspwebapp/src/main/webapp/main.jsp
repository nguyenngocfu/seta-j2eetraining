<%@ page import="com.seta.training.model.*" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<% 
	User user = (User)request.getAttribute("userInfo");
 %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Main page</title>
</head>
<body>
User info</body>
<br/>
<table>
	<tr>
		<td>User name</td>
		<td><%=user.getUsername() %></td>
	</tr>
		<tr>
		<td>User id</td>
		<td><%=user.getUserId() %></td>
	</tr>
	<tr>
		<td>First name</td>
		<td><%=user.getFirstName() %></td>
	</tr>
		<tr>
		<td>Last name</td>
		<td><%=user.getLastName() %></td>
	</tr>
</table>

<jsp:useBean id="address" class="com.seta.training.model.Address" scope="session"/>
<jsp:setProperty property="userId" name="address" value="<%=user.getUserId() %>"/>
<jsp:include page="address.jsp"></jsp:include>
</html>
