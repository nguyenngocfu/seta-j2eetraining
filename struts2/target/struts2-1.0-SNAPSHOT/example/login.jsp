<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="./javascript/jquery-1.11.2.min.js"></script>
<title>Login</title>
</head>
<body>

	<fieldset>
	<form action="LoginServlet" method="post">
		<div style="display: block;">
			Username : <input name="username" type="text" style="margin-left: 5dp" maxlength="50">
		</div>
		<div style="display: block;">
			Password : <input name="password" type="password" style="margin-left: 5dp" maxlength="50">
		</div>
		<input type="submit" value="Login" id="btnLogin">
	</form>
	</fieldset>
</body>
</html>