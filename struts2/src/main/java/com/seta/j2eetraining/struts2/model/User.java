package com.seta.j2eetraining.struts2.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="user", catalog="j2eetraining")
public class User implements Serializable{
	private int userId;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	
	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	@Column(name="user_name")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name="password")
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="user_id")
	public int getUserId() {
		System.out.println("Usser .... id " + userId);
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Column(name="firstname")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name="lastname")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
}

