package com.seta.j2eetraining.struts2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * @author: NgocNQ
 * Address model, has one to one relationship with User model
 */
@Entity
@Table(name="address", catalog = "j2eetraining")
public class Address {
	private int id;
	private int userId;
	private String address;
	private String city;
	private String state;
	private String country;
	
	public Address() {
		
	}
	
	public Address(int id, int userId, String address, String city,
			String state, String country) {
		super();
		this.id = id;
		this.userId = userId;
		this.address = address;
		this.city = city;
		this.state = state;
		this.country = country;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false, unique=true)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="user_id", nullable=false, unique=true)
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	@Column(name="address")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name="city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name="state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name="country")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

}
